package main

import (
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"msbigtech/internal/handlers"
)

// @title           MSBigtech Users API
// @version         1.0

// @host      msbigtech.local
// @BasePath /v1
// @schemes http https

// @tag.name friends
// @tag.description Friends API
// @tag.name profile
// @tag.description Profile API
// @tag.name healthchecks
// @tag.description Healthchecks API

// @securitydefinitions.oauth2.implicit MSBigtechAuth
// @authorizationUrl https://msbigtech.local/oauth/authorize
// @scope.read:friends get friends
// @scope.delete:friends modify friends
// @scope.add:friends add friend
// @scope.write:profile update profile

// @securitydefinitions.apikey api_key
// @in header
// @name api_key

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/live", handlers.LivenessProbeHandler)
	r.GET("/ready", handlers.ReadinessProbeHandler)

	r.GET("/search", handlers.SearchFriendsHandler)
	r.PUT("/profile", handlers.UpdateProfileHandler)
	r.PUT("/profile/avatar", handlers.UpdateProfileAvatarHandler)

	r.GET("/friends", handlers.GetCurrentUserFriendsHandler)
	r.POST("/friends/:friendId", handlers.AddFriendHandler)
	r.DELETE("/friends/:friendId", handlers.DeleteFriendHandler)
	r.POST("/friends/:friendId/accept", handlers.AcceptFriendshipHandler)
	r.POST("/friends/:friendId/decline", handlers.DeclineFriendshipHandler)

	return r
}

func goDotEnvVariable() {
	godotenv.Load(".env")
}

func main() {
	goDotEnvVariable()

	r := setupRouter()

	r.Run(":8080")
}
