package responses

// UpdateProfileResponse update profile response
//
// swagger:model UpdateProfileResponse
type UpdateProfileResponse struct {

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// nickname
	Nickname string `json:"nickname,omitempty"`
}
