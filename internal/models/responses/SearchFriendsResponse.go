package responses

import "msbigtech/internal/models"

// SearchFriendsResponse search friends response
//
// swagger:model SearchFriendsResponse
type SearchFriendsResponse struct {

	// items
	Items []*models.User `json:"items"`
}
