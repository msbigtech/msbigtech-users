package responses

import "msbigtech/internal/models"

// GetCurrentUserFriendsResponse get current user friends response
//
// swagger:model GetCurrentUserFriendsResponse
type GetCurrentUserFriendsResponse struct {

	// items
	Items []*models.User `json:"items"`
}
