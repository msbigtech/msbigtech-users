package responses

// ErrorResponse error response
//
// swagger:model ErrorResponse
type ErrorResponse struct {

	// message
	Message string `json:"message,omitempty"`
}
