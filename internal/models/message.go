package models

// Message message
//
// swagger:model Message
type Message struct {

	// author
	Author *User `json:"author,omitempty"`

	// created at
	CreatedAt string `json:"createdAt,omitempty"`

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// text
	Text string `json:"text,omitempty"`
}
