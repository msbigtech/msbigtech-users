package requests

// UpdateProfileAvatarRequest update profile avatar request
//
// swagger:model UpdateProfileAvatarRequest
type UpdateProfileAvatarRequest struct {

	// base64image
	Base64image string `json:"base64image,omitempty"`
}
