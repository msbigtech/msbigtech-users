package requests

// SearchFriendsRequest search friends request
//
// swagger:model SearchFriendsRequest
type SearchFriendsRequest struct {

	// friend name
	FriendName string `json:"friendName,omitempty"`
}
