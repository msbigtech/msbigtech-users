package requests

// UpdateProfileRequest update profile request
//
// swagger:model UpdateProfileRequest
type UpdateProfileRequest struct {

	// nickname
	Nickname string `json:"nickname,omitempty"`
}
