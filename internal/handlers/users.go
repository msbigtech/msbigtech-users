package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/files"
	_ "github.com/swaggo/gin-swagger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"msbigtech/internal/models/requests"
	"msbigtech/internal/models/responses"
	"net/http"
	"os"
	"strconv"
)

func httpErrorMsg(err error) *responses.ErrorResponse {
	if err == nil {
		return nil
	}

	return &responses.ErrorResponse{
		Message: err.Error(),
	}
}

// LivenessProbeHandler godoc
// @Summary      liveness probe
// @Tags         healthchecks
// @Accept       json
// @Produce      json
// @Success      200  {string}  string    "ok"
// @Router       /live [get]
func LivenessProbeHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"response": "ok",
	})
}

// ReadinessProbeHandler godoc
// @Summary      readiness probe
// @Tags         healthchecks
// @Accept       json
// @Produce      json
// @Success      200  {string}  string    "ok"
// @Router       /ready [get]
func ReadinessProbeHandler(c *gin.Context) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=prefer TimeZone=Europe/Moscow",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PORT"),
	)
	_, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "ok",
	})
}

// SearchFriendsHandler godoc
// @Summary      Выполняет поиск пользователей
// @Tags         friends
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:friends]
// @Param		 data	body	requests.SearchFriendsRequest  true "SearchFriendsRequest"
// @Success      200  {object} 	responses.SearchFriendsResponse "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /search [post]
func SearchFriendsHandler(c *gin.Context) {
	var request requests.SearchFriendsRequest

	if err := json.NewDecoder(c.Request.Body).Decode(&request); err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	response := responses.SearchFriendsResponse{}

	c.JSON(http.StatusOK, response)
}

// UpdateProfileHandler godoc
// @Summary      Редактирует профиль
// @Tags         profile
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[write:profile]
// @Param		 data	body	requests.UpdateProfileRequest  true "UpdateProfileRequest"
// @Success      200  {object} 	responses.UpdateProfileResponse "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /profile [put]
func UpdateProfileHandler(c *gin.Context) {
	var request requests.UpdateProfileRequest

	if err := json.NewDecoder(c.Request.Body).Decode(&request); err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	response := responses.UpdateProfileResponse{}

	c.JSON(http.StatusOK, response)
}

// UpdateProfileAvatarHandler godoc
// @Summary      Редактирует аватар профиля
// @Tags         profile
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[write:profile]
// @Param		 data	body	requests.UpdateProfileAvatarRequest  true "UpdateProfileAvatarRequest"
// @Success      200  {string}  string    "ok"
// @Router       /profile/avatar [put]
func UpdateProfileAvatarHandler(c *gin.Context) {
	var request requests.UpdateProfileAvatarRequest

	if err := json.NewDecoder(c.Request.Body).Decode(&request); err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	c.JSON(http.StatusOK, "")
}

// GetCurrentUserFriendsHandler godoc
// @Summary      Возвращает список друзей
// @Tags         friends
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:friends]
// @Success      200  {object} 	responses.GetCurrentUserFriendsResponse "successful operation"
// @Router       /friends [get]
func GetCurrentUserFriendsHandler(c *gin.Context) {
	// TODO

	response := responses.GetCurrentUserFriendsResponse{}

	c.JSON(http.StatusOK, response)
}

// AddFriendHandler godoc
// @Summary      Добавляет пользователя в друзья
// @Tags         friends
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[add:friends]
// @Param		 friendId		path	int		true	"ID of friend"
// @Success      200  {string}  string    "ok"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /friends/{friendId} [post]
func AddFriendHandler(c *gin.Context) {
	// TODO

	_, err := strconv.ParseInt(c.Param("friendId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	c.JSON(http.StatusOK, "")
}

// DeleteFriendHandler godoc
// @Summary      Удаляет пользователя из друзей
// @Tags         friends
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[delete:friends]
// @Param		 friendId		path	int		true	"ID of friend"
// @Success      200  {string}  string    "ok"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /friends/{friendId} [delete]
func DeleteFriendHandler(c *gin.Context) {
	// TODO

	_, err := strconv.ParseInt(c.Param("friendId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	c.JSON(http.StatusOK, "")
}

// AcceptFriendshipHandler godoc
// @Summary      Принять запрос на дружбу
// @Tags         friends
// @Accept       json
// @Produce      json
// @Param		 friendId		path	int		true	"ID of friend"
// @Success      200  {string}  string    "ok"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /friends/{friendId}/accept [post]
func AcceptFriendshipHandler(c *gin.Context) {
	_, err := strconv.ParseInt(c.Param("friendId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	c.JSON(http.StatusOK, "")
}

// DeclineFriendshipHandler godoc
// @Summary      Отклонить запрос на дружбу
// @Tags         friends
// @Accept       json
// @Produce      json
// @Param		 friendId		path	int		true	"ID of friend"
// @Success      200  {string}  string    "ok"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /friends/{friendId}/decline [post]
func DeclineFriendshipHandler(c *gin.Context) {
	_, err := strconv.ParseInt(c.Param("friendId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	c.JSON(http.StatusOK, "")
}
