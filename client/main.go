package users

import (
	"log"
	"net/http"
)

func searchUsersTest() {
	httpClient := &http.Client{}

	resp, err := searchUsers(httpClient, "Server")

	if err != nil {
		log.Printf("searchServers error: %v", err)
	} else {
		log.Printf("searchServers: %#v", resp)
	}
}

func main() {
	searchUsersTest()
}
