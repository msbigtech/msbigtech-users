package users

import (
	"bytes"
	"encoding/json"
	"fmt"
	"msbigtech/internal/generated"
	"net/http"
)

const host = "http://msbigtech.local"
const apiPrefix = "/api/v1/users"

func searchUsers(httpClient *http.Client, username string) (*generated.SearchFriendsResponse, error) {
	uri := fmt.Sprintf("%s/%s/%s", host, apiPrefix, "search")

	var request = generated.SearchFriendsRequest{
		FriendName: username,
	}

	body := bytes.NewBuffer(nil)
	if err := json.NewEncoder(body).Encode(request); err != nil {
		return nil, err
	}

	httpRequest, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		return nil, err
	}

	httpResponse, err := httpClient.Do(httpRequest)
	if err != nil {
		return nil, err
	}

	response := new(generated.SearchFriendsResponse)
	if err := json.NewDecoder(httpResponse.Body).Decode(response); err != nil {
		return nil, fmt.Errorf("can't unmarshal reponse: %s", err.Error())
	}

	return response, nil
}
